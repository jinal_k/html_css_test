var assert = require('assert');

function calculateSimpleInterest(arr){
    let result = arr.map(x=> x = ((x.p)*(x.r)*(x.t))/100);
    result = result.filter(x => x > 0);
    return result;
}

const test = [
    {
      "p": 73,
      "r": 98,
      "t": 11
    },
    {
      "p": 25,
      "r": 37,
      "t": 77
    },
    {
      "p": 34,
      "r": -44,
      "t": 69
    },
    {
      "p": 86,
      "r": 25,
      "t": 27
    },
    {
      "p": 27,
      "r": -38,
      "t": 11
    },
    {
      "p": 4,
      "r": 11,
      "t": 66
    }
]


const test1 = [
    {
        "p": 5,
        "r": -4,
        "t": 0
    },
    {
        "p": 500,
        "r": 4,
        "t": 1
    },
    {
        "p": 5,
        "r": 4,
        "t": 1000
    }
]

assert.deepEqual(calculateSimpleInterest(test),[ 786.94, 712.25, 580.5, 29.04 ]);
assert.deepEqual(calculateSimpleInterest(test1),[20, 200]);