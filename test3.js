class DoublyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    addNode(data, position = this.length) {
        let node = new this.Node(data);
        if (this.head == null) {
            this.head = node;
            this.tail = node;
            this.length++;
            return this.head;
        }

        if (position == 0) {
            node.prev = null;
            node.next = this.head;
            this.head.prev = node;
            this.head = node;
            this.length++;
            return this.head;
        }

        if(position != this.length){
            let temp = this.head;
            let cur = 1;
            while (temp.next != null && cur<position){
                temp = temp.next;
                cur++;
            }
            if(temp.next != null){
                node.next = temp.next;
                temp.next.prev = node;
                temp.next = node;
                node.prev = temp;
            }
            else{
                node.prev = temp;
                temp.next = node;
            }
        }
        else{
            let temp = this.head;
            while (temp.next != null){
                temp = temp.next;
            }
            temp.next = node;
            node.prev = temp;
            this.length++;
        }
        
    }
    display() {
        let currNode = this.head;
        while (currNode != null) {
           console.log(currNode.data + " <-> ");
           currNode = currNode.next;
        }
     }
}

DoublyLinkedList.prototype.Node = class {
    constructor(data) {
       this.data = data;
       this.next = null;
       this.prev = null;
    }
 };


const dll = new DoublyLinkedList();
dll.addNode(1);
dll.addNode(7);
dll.addNode(3);
dll.addNode(9,0);
dll.addNode(4,2);
dll.display();
